/*
 * Copyright (C) 2018 Amazon.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 */

/dts-v1/;

#include <dt-bindings/input/input.h>
#include "checkers.dtsi"

/ {
	version = "evt";
};

&kd_camera_hw1 {
        reg-vcamd-supply = <&mt6323_vgp2_reg>;
	cam-pwr-en-gpios = <&pio 45 GPIO_ACTIVE_HIGH>;
};

&touch {
	vtouch-supply = <&mt6323_vgp1_reg>;
};

&pwrap {
	pmic: mt6323 {
		regulators {
			mt6323_vgp1_reg: ldo_vgp1 {
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
				regulator-boot-on;
			};

			mt6323_vgp2_reg: ldo_vgp2 {
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <3300000>;
				/delete-property/ regulator-always-on;
				/delete-property/ regulator-boot-on;
			};
		};
	};
};

&gpio_keys {
	camera_lens_cover {
		label = "Camera Lens Cover";
		debounce-interval = <50>;
		gpios = <&pio 142 GPIO_ACTIVE_LOW>;
		linux,input-type = <EV_SW>;
		linux,code = <SW_CAMERA_LENS_COVER>;
	};
};

&i2c0 {
	tlv320aic3101@19 {
		status = "disabled";
	};
};

&i2c2 {
	/delete-node/ focaltech_ts@38;
};
